from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Forms
from .models import Jadwal

response={}
def saveact(request):
	form = Forms(request.POST or None )
	if(request.method == 'POST' and form.is_valid()):
		response['day'] = request.POST['day']
		response['date'] = request.POST['date']
		response['time'] = request.POST['time']
		response['activity'] = request.POST['activity']
		response['place'] = request.POST['place']
		response['category'] = request.POST['category']
		jadwal = Jadwal(day=response['day'], date=response['date'], time=response['time'], activity=response['activity'], place=response['place'], category=response['category'])
		jadwal.save()
		return HttpResponseRedirect('/story5/form_result5')
	else:
		return HttpResponseRedirect('/story5/form_field5')

def fillact(request):
	response['listform'] = Forms
	return render(request, 'form_field5.html', response)

def showact(request):
	jadwal = Jadwal.objects.all()
	response['jadwal'] = jadwal
	return render(request, 'form_result5.html', response)

def delete(request):
    obj = Jadwal.objects.all().delete()
    return HttpResponseRedirect('/story5/form_result5')