from django.conf.urls import url

from .views import saveact
from .views import fillact
from .views import showact
from .views import delete

app_name = 'story5'

urlpatterns = [
	url(r'^saveact', saveact, name='saveact'),
	url(r'^form_result', showact, name='form_result'),
	url(r'^form_field', fillact),
	url(r'^delete', delete, name='delete'),
]