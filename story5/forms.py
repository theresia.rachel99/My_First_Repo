from django import forms

class Forms(forms.Form):
    attrs = {
        'type': 'text',
        'class': 'form-control'
    }

    OPTION= (
        ('Akademis', 'Akademis'),
        ('Kepanitiaan', 'Kepanitiaan'),
        ('Hiburan', 'Hiburan')
    )

    day = forms.CharField(label="Day", max_length=6, widget=forms.TextInput(attrs=attrs))
    date = forms.DateField(label="Date", required=True, widget=forms.DateInput(attrs={'class': 'form-control','type': 'date'} ))
    time = forms.TimeField(label="Time", required=True, widget=forms.TimeInput(attrs= {'class': 'form-control', 'type': 'time'} ))
    activity = forms.CharField(label="Activity", max_length=27, required=True, widget=forms.TextInput(attrs=attrs))
    place = forms.CharField(label="Place", max_length=27, required=True, widget=forms.TextInput(attrs=attrs))
    category = forms.ChoiceField(choices=OPTION)
