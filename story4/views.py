from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Forms
from .models import Jadwal


def home(request):
	context={'home':'active'}
	return render(request, 'home.html', context)
	
def profile(request):
	context={'profile':'active'}
	return render(request, 'profile.html', context)
	
def experiences(request):
	context={'experiences':'active'}
	return render(request, 'experiences.html', context)

def skills(request):
	context={'skills':'active'}
	return render(request, 'skills.html', context)
	
def	contacts(request):
	context={'contacts':'active'}
	return render(request, 'contacts.html', context)
	
def form(request):
	context={'form':'active'}
	return render(request, 'form.html', context)

response={}
def saveact(request):
	form = Forms(request.POST or None )
	if(request.method == 'POST' and form.is_valid()):
		response['day'] = request.POST['day']
		response['date'] = request.POST['date']
		response['time'] = request.POST['time']
		response['activity'] = request.POST['activity']
		response['place'] = request.POST['place']
		response['category'] = request.POST['category']
		jadwal = Jadwal(day=response['day'], date=response['date'], time=response['time'], activity=response['activity'], place=response['place'], category=response['category'])
		jadwal.save()
		return HttpResponseRedirect('/form_result.html')
	else:
		return HttpResponseRedirect('/form_field.html')

def fillact(request):
	response['saveact'] = form
	return render(request, 'form_field.html', response)

def showact(request):
	jadwal = Jadwal.objects.all()
	response['jadwal'] = jadwal
	return render(request, 'form_result.html', response)


