from django.db import models

class Jadwal(models.Model):
    day = models.CharField(max_length=27)
    date = models.DateField()
    time = models.TimeField()
    activity = models.CharField(max_length=27)
    place = models.CharField(max_length=27)
    category = models.CharField(max_length=27)

