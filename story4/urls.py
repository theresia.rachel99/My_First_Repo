from django.conf.urls import url

from .views import home
from .views import profile
from .views import experiences
from .views import skills
from .views import contacts
from .views import form
from .views import saveact
from .views import fillact
from .views import showact

app_name = 'story4'

#url for app
urlpatterns = [
	url(r'^home', home, name='home'),
	url(r'^profile', profile, name='profile'),
	url(r'^experiences', experiences, name='experiences'),
	url(r'^skills', skills, name='skills'),
	url(r'^contacts', contacts, name='contacts'),
	url(r'^form', form, name='form'),
	url(r'^saveact', saveact, name='saveact'),
	url(r'^form_result', showact, name='form_result'),
	url(r'^form_field', fillact),
	url(r'', home, name='home')
]